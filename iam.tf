#### IAM Role for Lambda #####

resource "aws_iam_role" "iam_for_lambda" {
  name = "AMI_Unshare_Lambda_Role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = var.default_tags
}




###### Policy Attached to Lambda Function ######

# EC2 Policy Attachment

resource "aws_iam_role_policy_attachment" "ami_attach" {
  role      = aws_iam_role.iam_for_lambda.name
  policy_arn = data.aws_iam_policy.ec2.arn
}

# Basic Lambda Exectuion Policy & Logs

resource "aws_iam_role_policy_attachment" "logs_attach" {
  role      = aws_iam_role.iam_for_lambda.name
  policy_arn = data.aws_iam_policy.logs.arn
}

# SNS Policy Attachment

resource "aws_iam_role_policy_attachment" "sns_attach" {
  role      = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.sns_pub.arn
}

### SNS Pub Policy ###

resource "aws_iam_policy" "sns_pub" {
  name        = "ami_unshare_sns_pub"
  description = "Policy to allow Lambda to Publish Results to SNS Topic"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "Stmt1623233447497",
        "Action" : [
          "sns:Publish"
        ],
        "Effect" : "Allow",
        "Resource" : "*"
      }
    ]
  })
}