# Shared Services - AWS AMI Unsharing Tool

Module for the creation of a Python Lambda function and all related collateral. 

The AMI Unsharing Python Function is used to Unshare AMI's with permissions to other accounts after a set amount of days. Shared Services faced the issue of high costs associated to old AMI's and their snapshots. Shared Services routinely patch and provide Gold Images for AO tech to utilise, so to increase cost savings and security we are going to archive any AMI's older than 60 days. 

Any Images with a 'DoNotUnshare' tag with a value of 'true' will be excluded from the function (for images you want to keep shared out that are older than the set amount of days)

This Module allows you to deploy the same python function, allowing you to specify the retention period on days, alongside providing feedback via the form of emails for successful/failed executions. 


## Usage  >v0.13 - Lambda Function - AMI Unshare

### Main.tf

```terraform

module "ami_unshare" {
  source                      = "https://gitlab.com/ao-world-plc-open/terraform-aws-ami_unshare.git?ref=v1.0.0"
  IMAGES_OLDER_THAN           = "30"
  notification_email_address  = "test@ao.com"
}
```
## Variables

- `IMAGES_OLDER_THAN`                            - Number of Days - Default is 60
- `notification_email_address`                   - Email Address to be Notified at. 