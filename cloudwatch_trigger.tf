#### Cron Rule Running Daily @ 12 AM ####

resource "aws_cloudwatch_event_rule" "cron" {
  name                = "cron_rule"
  schedule_expression = "cron(0 0 ? * * *)"
  description         = "AMI Unshared Lambda to run Once Daily"
  tags                = var.default_tags
}

### AWS Cloudwatch Event Target

resource "aws_cloudwatch_event_target" "ami_unshare" {
  rule = aws_cloudwatch_event_rule.cron.name
  arn  = aws_lambda_function.ami_unshare.arn
}


### AWS Lambda Rule Permission #####

resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ami_unshare.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cron.arn
}