variable "IMAGES_OLDER_THAN" {
  default = 60
  description = "Images Older than X Amount of Days to be Unshared"
}


variable "default_tags" {
  description = "Default Tags to apply to resources"
}

variable "notification_email_address" {
  description = "Email Address To Notify of Successful/Failed Lambda Executions"
}