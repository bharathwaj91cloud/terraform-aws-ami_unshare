#### Lambda Function ####

resource "aws_lambda_function" "ami_unshare" {
  filename         = "${path.module}/files/ami_unshare.zip"
  function_name    = "AMI_Unshare"
  role             = aws_iam_role.iam_for_lambda.arn
  handler          = "ami_unshare.lambda_handler"
  source_code_hash = filebase64sha256("${path.module}/files/ami_unshare.zip")
  timeout          = 180

  runtime = "python3.8"

  environment {
    variables = {
      IMAGES_OLDER_THAN = var.IMAGES_OLDER_THAN
    }
  }
}

### AWS Lmabda Alias ####

resource "aws_lambda_alias" "ami_unshare" {
  name             = "ami_unshare"
  description      = "AWS Lambda Function to Unshare Images Over X Amount of Days"
  function_name    = aws_lambda_function.ami_unshare.arn
  function_version = aws_lambda_function.ami_unshare.version
}


#### Notfications on Success & Failure ####

resource "aws_lambda_function_event_invoke_config" "sns_destination" {
  function_name = aws_lambda_alias.ami_unshare.function_name

  destination_config {
    on_failure {
      destination = aws_sns_topic.lambda_updates.arn
    }

    on_success {
      destination = aws_sns_topic.lambda_updates.arn
    }
  }
}