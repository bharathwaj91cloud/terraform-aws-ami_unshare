
#### Data Look Up for EC2 Full Admin Policy for Lambda to be able to Query and Remove Unwanted AMI's ######

data "aws_iam_policy" "ec2" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

data "aws_iam_policy" "logs" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

### Current AWS Account ID ####

data "aws_caller_identity" "current" {}
